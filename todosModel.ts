class TODOS {
    public items: string[];

    constructor(items: string[]) {
        this.items = items;
    };

    add(list : todoItem) {
        //Add TODO
        this.items.push('{"id":'+list.id+', "detail":"'+list.detail+'", "checkStatus":"'+list.checkStatus+'"}');

        // Update localStorage
        this.updateLocalStorage();

        //Update UI
        this.updateUI();
    };

    display() {
        console.log(`items ${this.items} `);
    };

    // delete to-do item
    deleteTodo(id:number){
        var newTodoItems:string[] = [];
        var idx: number = 0;

        for(var i=0;i<todos.items.length;i++){
            if(JSON.parse(todos.items[i]).id!=id){
                newTodoItems[idx] = todos.items[i];
                idx++;
            }
        }
        this.items = newTodoItems;

        // if all todos deleted then set ID in localstorage to 0
        if(newTodoItems.length==0){
            localStorage.setItem('id', "1");
        }
        this.updateLocalStorage();

        //Update UI
        this.updateUI();
    };

    updateLocalStorage(){
        localStorage.setItem('todos', JSON.stringify(this.items));
    };

    updateUI(){
        document.getElementById('todoList').innerHTML='';
        document.getElementById('todoList').appendChild( makeUL(getItems()));
    };
};

var todos;
if(localStorage.getItem('todos')){
    todos = new TODOS(JSON.parse(localStorage.getItem('todos')));
}
else{
    // Initial setup, if not even a single TO-DO has been added yet
    todos = new TODOS([ '{ "id" : 1, "detail": "Learn Angular", "checkStatus": "incomplete"}' ]);
    localStorage.setItem('todos', JSON.stringify(todos.items));
    localStorage.setItem('id', "2");
}

// add to-do item
function addItem(detail: string) {

    var id: number;
    if(localStorage.getItem('id')){
        id = parseInt(localStorage.getItem('id'));
        var newId: number;
        newId = id +1;
        localStorage.setItem('id',newId.toString());
    }
    else{
        console.log('PROBLEMMMMMMMM');
    }

    todos.add({
        id: id,
        detail: detail,
        checkStatus: 'incomplete'
    });
};

function displayItem() {
    todos.display();
};

function getItems(){
    return todos.items;
};