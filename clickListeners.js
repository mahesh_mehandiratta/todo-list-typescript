// edit button click listener
function todoItemEditButton(id) {
    var button = document.getElementById("editButton" + id);
    var respectiveDiv = document.getElementById("div" + id);
    if (button.innerText == 'Edit') {
        respectiveDiv.contentEditable = "true";
        button.innerText = 'Save';
        button.setAttribute("class", "btn btn-success");
    }
    else {
        button.innerText = 'Edit';
        respectiveDiv.contentEditable = "false";
        button.setAttribute("class", "btn btn-primary");
        for (var i = 0; i < todos.items.length; i++) {
            var itemJSON = JSON.parse(todos.items[i]);
            if (itemJSON.id == id) {
                itemJSON.detail = respectiveDiv.innerText;
                todos.items[i] = JSON.stringify(itemJSON);
            }
        }
        todos.updateLocalStorage();
        todos.updateUI();
    }
}
;
// delete button click listener
function todoItemDeleteButton(id) {
    todos.deleteTodo(id);
}
;
// complete-incomplete to-do button click listener
function todoItemCheckUncheckButton(id) {
    var listItem = document.getElementById(id);
    if (listItem.getAttribute("checkStatus") == "complete") {
        listItem.setAttribute("checkStatus", "incomplete");
    }
    else {
        listItem.setAttribute("checkStatus", "complete");
    }
    for (var i = 0; i < todos.items.length; i++) {
        var itemJSON = JSON.parse(todos.items[i]);
        if (itemJSON.id == id) {
            itemJSON.checkStatus = listItem.getAttribute("checkStatus");
            todos.items[i] = JSON.stringify(itemJSON);
        }
    }
    todos.updateLocalStorage();
    todos.updateUI();
}
;
function todoItemClickListener(id) {
    console.log(id);
}
;
