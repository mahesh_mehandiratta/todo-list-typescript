### What is this repository for? ###

* Made by: Mahesh Mehandiratta (Employee code: 3146489)
* This is code for submission of TODO-List made in typescript
* All features ( add/delete/edit/mark as complete-incomplete have been made)
* Check demo here: http://biochemist-alcyon-14764.bitballoon.com/

### How do I get set up? ###

* Download zip of code and extract it
* Open project in your IDE
* Run "npm install"
* Open index.html in browser
* Type text in topmost input box and click "Add TODO" to add todo
* Other functions work as show by buttons