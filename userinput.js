window.onload = function () {
    var input = document.getElementById('inp');
    var btn = document.getElementById('btn');
    btn.onclick = function () {
        var value = input.value;
        if (value.trim() == '') {
            alert('Empty todo not allowed');
        }
        else {
            addItem(value.trim());
        }
        displayItem();
    };
    document.getElementById('todoList').appendChild(makeUL(getItems()));
};
// Create the <li> list element inside <ul> for each to-do saved in localStorage
function makeUL(array) {
    var list = document.createElement('ul');
    array.forEach(function (itemString) {
        var itemJSON = JSON.parse(itemString);
        var item = document.createElement('li');
        item.setAttribute("id", itemJSON.id.toString());
        item.setAttribute("checkStatus", itemJSON.checkStatus.toString());
        item.setAttribute("style", "word-wrap: break-word;");
        item.setAttribute("class", "listItem");
        var editButton = document.createElement("button");
        editButton.innerHTML = "Edit";
        editButton.setAttribute('onclick', 'todoItemEditButton(' + itemJSON.id.toString() + ')');
        editButton.setAttribute("id", "editButton" + itemJSON.id.toString());
        editButton.setAttribute("class", "button btn btn-primary");
        var deleteButton = document.createElement("button");
        deleteButton.innerHTML = "Delete";
        deleteButton.setAttribute('onclick', 'todoItemDeleteButton(' + itemJSON.id.toString() + ')');
        deleteButton.setAttribute("id", "deleteButton" + itemJSON.id.toString());
        deleteButton.setAttribute("class", "button btn btn-danger");
        var checkUncheckButton = document.createElement("button");
        if (itemJSON.checkStatus == "complete") {
            checkUncheckButton.innerHTML = "Mark as incomplete";
            checkUncheckButton.setAttribute("class", "button btn btn-warning");
        }
        else {
            checkUncheckButton.innerHTML = "Mark as complete";
            checkUncheckButton.setAttribute("class", "button btn btn-success");
        }
        checkUncheckButton.setAttribute('onclick', 'todoItemCheckUncheckButton(' + itemJSON.id.toString() + ')');
        checkUncheckButton.setAttribute("id", "checkUncheckButton" + itemJSON.id.toString());
        var div = document.createElement("div");
        div.contentEditable = "false";
        div.innerHTML = itemJSON.detail;
        div.setAttribute("id", "div" + itemJSON.id.toString());
        item.appendChild(div);
        item.appendChild(editButton);
        item.appendChild(deleteButton);
        item.appendChild(checkUncheckButton);
        list.appendChild(item);
    });
    return list;
}
